from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.shortcuts import render


urlpatterns = patterns('',
    url(r'^$', 'apps.exercises.views.home', name='home'),

    url(r'^', include('apps.accounts.urls', namespace='accounts')),
    url(r'^', include('apps.exercises.urls', namespace='exercises')),
    url(r'^', include('apps.diets.urls', namespace='diets')),
    url(r'^', include('apps.progress.urls', namespace='progress')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^html/(?P<template_name>.*\.html)$', (lambda request, template_name: render(request, template_name))),
    )
