# coding: utf-8
from django.contrib import admin


from .models import ExerciseProgress


@admin.register(ExerciseProgress)
class ExerciseProgressAdmin(admin.ModelAdmin):
    list_display = ('exercise', 'sets', 'repetitions', 'video_viewed', )


from .models import Badge


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'description', 'icon', )


from .models import Achievement


@admin.register(Achievement)
class AchievementAdmin(admin.ModelAdmin):
    list_display = ('user', 'badge', 'received_time', )
    pass


from .models import Statement

@admin.register(Statement)
class StatementAdmin(admin.ModelAdmin):
    list_display = ('user', 'number', 'statement_type', 'request_time', )
    pass
