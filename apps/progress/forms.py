# coding: utf-8
from django import forms
from .models import ExerciseProgress


class ProgressForm(forms.ModelForm):

    class Meta:
        model = ExerciseProgress
        fields = ('sets', 'repetitions',)
