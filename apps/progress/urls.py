from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.progress.views',
    url(r'^api/trainer/$', 'get_trainer', name='trainer'),
    url(r'^api/trainers/$', 'get_trainers', name='trainers'),
    url(r'^api/trainer/exercises/$', 'get_trainer_exercises', name='trainer_exercises'),
    url(r'^api/online/set/$', 'set_time_online', name='time_online'),
    url(r'^api/video/viewed/$', 'video_viewed', name='video_viewed'),
    url(r'^api/diet/viewed/$', 'diet_viewed', name='diet_viewed'),
    url(r'^api/burned/$', 'get_burned_calories', name='burned_calories'),
    url(r'^api/share/$', 'share_result', name='share_result'),
    url(r'^progress/exercise/(?P<exercise_id>[\w]+)/$', 'progress_exercise', name='progress_exercise'),
)