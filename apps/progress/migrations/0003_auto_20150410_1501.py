# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('exercises', '0001_initial'),
        ('progress', '0002_achievement'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShareDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField(verbose_name='\u0434\u0435\u043d\u044c')),
                ('action_time', models.DateTimeField(auto_now_add=True, verbose_name='\u0432\u0440\u0435\u043c\u044f \u0448\u0430\u0440\u044b')),
                ('trainer', models.ForeignKey(to='exercises.Trainer')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Statement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0437\u0430\u044f\u0432\u043a\u0438')),
                ('request_time', models.DateTimeField(auto_now_add=True, verbose_name='\u0432\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f \u0437\u0430\u044f\u0432\u043a\u0438')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='achievement',
            options={'ordering': ['id'], 'verbose_name': '\u0414\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0414\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterModelOptions(
            name='badge',
            options={'ordering': ['id'], 'verbose_name': '\u0411\u0435\u0439\u0434\u0436 \u0437\u0430 \u0414\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0411\u0435\u0439\u0434\u0436\u0438 \u0437\u0430 \u0414\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u044f'},
        ),
    ]
