# coding: utf-8
from django.contrib import messages
from django.shortcuts import render, redirect
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.db.utils import IntegrityError

from apps.accounts.views import get_day_user_in_system
from apps.accounts.models import User
from apps.exercises.models import Trainer, Exercise
from apps.diets.models import Diet, DietViewed
from .models import ExerciseProgress, Badge, Achievement, Statement
from .forms import ProgressForm


def get_trainers(request):
    # params: no params
    trainers = Trainer.objects.all()
    return JsonResponse({'trainers': list(trainers.values())})


def get_trainer(request):
    # params: trainer_id
    trainer_id = request.GET.get('id')
    if not id:
        return JsonResponse({'error': 'no trainer id'})

    try:
        trainer = Trainer.objects.get(id=trainer_id)
    except Trainer.DoesNotExist:
        return JsonResponse({'error': 'trainer does not exist'})
    else:
        return JsonResponse({'trainer': model_to_dict(trainer)})


def get_trainer_exercises(request):
    # params: trainer_id
    trainer_id = request.GET.get('trainer_id')
    if not id:
        return JsonResponse({'error': 'no trainer id'})

    try:
        trainer = Trainer.objects.get(id=trainer_id)
        exercises = Exercise.objects.filter(trainer=trainer).all()
    except Trainer.DoesNotExist:
        return JsonResponse({'error': 'trainer does not exist'})
    else:
        return JsonResponse({'exercises': list(exercises.values())})


def get_burned_calories(request):
    day = get_day_user_in_system(request.user)

    try:
        progresses = ExerciseProgress.objects.filter(user=request.user)
    except:
        progresses = []

    daily = 0
    total = 0
    for progress in progresses:
        total += float(progress)
        if progress.exercise.day == day:
            daily += float(progress)
    return JsonResponse({
        'daily': daily,
        'total': total,
    })


def record_badge(user, badge=None):
    if badge:
        achievement = Achievement(user=user, badge=badge)
        achievement.save()


def rating_tender(user):
    # подать заявку(автомат)
    statements = Statement.objects.filter(user=user)
    if len(statements) == 6:
        return
    statements_quantity = len(statements)

    # 1-4 дни
    exercises = Exercise.objects.filter(day=1)
    progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
    if len(progresses) > 1:
        try:
            statement = Statement.objects.get(user=user, statement_type='day1')
        except Statement.DoesNotExist:
            statement = Statement(user=user, statement_type='day1')
            statement.number = statements_quantity + 1
            statement.save()
            statements_quantity += 1

    exercises = Exercise.objects.filter(day=2)
    progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
    if len(progresses) > 1:
        try:
            statement = Statement.objects.get(user=user, statement_type='day2')
        except Statement.DoesNotExist:
            statement = Statement(user=user, statement_type='day2')
            statement.number = statements_quantity + 1
            statement.save()
            statements_quantity += 1

    exercises = Exercise.objects.filter(day=3)
    progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
    if len(progresses) > 1:
        try:
            statement = Statement.objects.get(user=user, statement_type='day3')
        except Statement.DoesNotExist:
            statement = Statement(user=user, statement_type='day3')
            statement.number = statements_quantity + 1
            statement.save()
            statements_quantity += 1

    exercises = Exercise.objects.filter(day=4)
    progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
    if len(progresses) > 1:
        try:
            statement = Statement.objects.get(user=user, statement_type='day4')
        except Statement.DoesNotExist:
            statement = Statement(user=user, statement_type='day4')
            statement.number = statements_quantity + 1
            statement.save()
            statements_quantity += 1

    # каждые 5 бейджей
    try:
        achievements = Achievement.objects.filter(user=user)
        if len(achievements) >= 5:
            try:
                statement = Statement.objects.get(user=user, statement_type='5badges')
            except Statement.DoesNotExist:
                statement = Statement(user=user, statement_type='5badges')
                statement.number = statements_quantity + 1
                statement.save()
                statements_quantity += 1

            if len(achievements) >= 10:
                try:
                    statement = Statement.objects.get(user=user, statement_type='5badges2')
                except Statement.DoesNotExist:
                    statement = Statement(user=user, statement_type='5badges2')
                    statement.number = statements_quantity + 1
                    statement.save()
                    statements_quantity += 1
    except Achievement.DoesNotExist:
        pass


def recalc_achievements(user, is_share=False):
    progresses = ExerciseProgress.objects.filter(user=user)
    days = {}
    for progress in progresses:
        day = progress.exercise.day
        if days.get(str(day)):
            days[str(day)] += 1
        else:
            days[str(day)] = 1

    filtered = []

    def filter(k, v):
        if v >= 2:
            filtered.append(int(k))
    trash = map(lambda i: filter(i, days[i]), days)
    filtered.sort()

    maxchain = 1
    chain = 1
    i = 1
    while i < len(filtered):
        if filtered[i - 1] + 1 == filtered[i]:
            chain += 1
        else:
            if chain > maxchain:
                maxchain = chain
            chain = 0
        i += 1

    # после 1го дня
    if maxchain > 0:
        badge = Badge.objects.get(code='pioneer')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            achievement = Achievement(user=user, badge=badge)
            achievement.save()

    # после 2х дней подряд
    if maxchain > 1:
        badge = Badge.objects.get(code='purposeful')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            achievement = Achievement(user=user, badge=badge)
            achievement.save()

    # после 3х дней подряд
    if maxchain > 2:
        badge = Badge.objects.get(code='seeker')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            achievement = Achievement(user=user, badge=badge)
            achievement.save()

    # после 4х дней подряд
    if maxchain > 3:
        badge = Badge.objects.get(code='victress')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            achievement = Achievement(user=user, badge=badge)
            achievement.save()

    # после 7ми дней подряд
    if maxchain > 6:
        badge = Badge.objects.get(code='celebrity')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            achievement = Achievement(user=user, badge=badge)
            achievement.save()

    # после 14ти дней подряд
    if maxchain > 13:
        badge = Badge.objects.get(code='legend')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            achievement = Achievement(user=user, badge=badge)
            achievement.save()

    # просмотреть программу питания в течение 2 любых дней
    badge = Badge.objects.get(code='cheerleader')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        try:
            progress = DietViewed.objects.filter(user=user)
        except DietViewed.DoesNotExist:
            pass
        else:
            if len(progress) >= 2:
                record_badge(user, badge)

    # заниматься больше 2х часов
    badge = Badge.objects.get(code='fitness master')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        if user.minutes_online > 120:
            record_badge(user, badge)

    # просмотреть до конца 10 упражнений за всё время
    badge = Badge.objects.get(code='record holder')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        try:
            progress = ExerciseProgress.objects.filter(user=user, video_viewed=True)
        except ExerciseProgress.DoesNotExist:
            pass
        else:
            if len(progress) >= 10:
                badge = Badge.objects.get(code='record holder')
                record_badge(user, badge)

    # сожжено всего 700 калорий
    badge700 = Badge.objects.get(code='fitnes guru')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge700)
    except Achievement.DoesNotExist:
        try:
            progress = ExerciseProgress.objects.filter(user=user)
        except ExerciseProgress.DoesNotExist:
            pass
        else:
            sum = 0
            for p in progress:
                sum += float(p)
            if sum > 700:
                record_badge(user, badge700)

    # сожжено всего 2к калорий
    badge2k = Badge.objects.get(code='champion')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge2k)
    except Achievement.DoesNotExist:
        try:
            progress = ExerciseProgress.objects.filter(user=user)
        except ExerciseProgress.DoesNotExist:
            pass
        else:
            sum = 0
            for p in progress:
                sum += float(p)
            if sum > 2000:
                record_badge(user, badge2k)

    # пользователь зашарил результат дня
    badge = Badge.objects.get(code='fitnes abonement')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        if is_share:
            record_badge(user, badge)

    # сгенерировать заявки
    rating_tender(user)


def set_time_online(request):
    # params: user_id[, time]
    user_id = request.POST.get('user_id')
    if not user_id:
        return JsonResponse({'error': 'no user id'})
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return JsonResponse({'error': 'wrong user id'})
    time = request.POST.get('time', 0)

    user.minutes_online += int(time)
    user.save()

    recalc_achievements(user)

    return JsonResponse({'time': 'setted'})


def video_viewed(request):
    # просмотры упражнений
    # params: user_id, exercise_id[, done]
    user_id = request.POST.get('user_id')
    if not user_id:
        return JsonResponse({'error': 'no user id'})
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return JsonResponse({'error': 'wrong user id'})

    exercise_id = request.POST.get('exercise_id')
    if not exercise_id:
        return JsonResponse({'error': 'no exercise id'})
    try:
        exercise = Exercise.objects.get(id=exercise_id)
    except Exercise.DoesNotExist:
        return JsonResponse({'error': 'wrong exercise id'})

    try:
        exercise_viewed = ExerciseProgress.objects.get(exercise=exercise, user=user)
    except ExerciseProgress.DoesNotExist:
        exercise_viewed = ExerciseProgress(exercise=exercise, user=user)
    done = request.POST.get('done', 0)
    origin = done
    done = True if done > 0 else False
    exercise_viewed.video_viewed = done
    exercise_viewed.save()

    recalc_achievements(user)

    return JsonResponse({'video': 'viewed=%s' % origin})


def diet_viewed(request):
    # params: user_id, diet_id[, done]
    user_id = request.POST.get('user_id')
    if not user_id:
        return JsonResponse({'error': 'no user id'})
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return JsonResponse({'error': 'wrong user id'})

    diet_id = request.POST.get('diet_id')
    if not diet_id:
        return JsonResponse({'error': 'no diet id'})
    try:
        diet = Diet.objects.get(id=diet_id)
    except User.DoesNotExist:
        return JsonResponse({'error': 'wrong diet id'})

    try:
        diet_viewed = DietViewed.objects.get(diet=diet, user=user)
    except DietViewed.DoesNotExist:
        diet_viewed = DietViewed(diet=diet, user=user)
    done = request.POST.get('done', 0)
    origin = done
    done = True if done > 0 else False
    diet_viewed.viewed = done
    diet_viewed.save()

    recalc_achievements(user)

    return JsonResponse({'diet': 'viewed=%s' % done})


def share_result(request):
    # params: user_id
    user_id = request.POST.get('user_id')
    if not user_id:
        return JsonResponse({'error': 'no user id'})
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return JsonResponse({'error': 'wrong user id'})

    recalc_achievements(user, is_share=True)
    rating_tender(user)

    return JsonResponse({'share': '1'})


class ProgressExerciseView(View):

    def get(self, request, exercise_id):
        progress = None
        if exercise_id:
            try:
                progress = ExerciseProgress.objects.get(user=request.user, exercise_id=exercise_id)
            except ExerciseProgress.DoesNotExist:
                progress = ExerciseProgress(user=request.user, exercise_id=exercise_id)

        form = ProgressForm(instance=progress)
        return render(request, 'progress.html', {
            'user': request.user,
            'form': form,
        })

    def post(self, request, exercise_id):
        form = ProgressForm(request.POST)
        if form.is_valid():
            try:
                exercise = Exercise.objects.get(id=exercise_id)
                progress = ExerciseProgress.objects.get(user=request.user, exercise=exercise)
            except Exercise.DoesNotExist:
                msg = 'Такого упражнения не существует.'
                messages.error(request, msg)
                return redirect(reverse('home'))
            except ExerciseProgress.DoesNotExist:
                progress = ExerciseProgress(user=request.user, exercise_id=exercise_id)

            exercise_sets = int(request.POST.get('sets'))
            exercise_repetitions = int(request.POST.get('repetitions'))
            if exercise_sets > 2 * exercise.prescription_sets or exercise_repetitions > 2 * exercise.prescription_repetitions:
                msg = 'При выполнении упражнения нужно следовать рекомендациям. Не допускайте превышения повторов и подходов. Данные не сохранены.'
                messages.error(request, msg)
                return redirect(reverse('exercises:exercises', args=(progress.exercise.trainer.id,)))
            progress.sets = exercise_sets
            progress.repetitions = exercise_repetitions

            try:
                progress.save()
            except IntegrityError:
                msg = 'Такого упражнения не существует'
                messages.error(request, msg)
                return redirect(reverse('home'))
            else:
                recalc_achievements(request.user)

            msg = 'Изменения сохранены.'
            messages.info(request, msg)

            recalc_achievements(request.user)

            return redirect(reverse('exercises:exercises', args=(progress.exercise.trainer.id,)))
        return render(request, 'progress.html', {
            'user': request.user,
            'form': form,
        })


progress_exercise = ProgressExerciseView.as_view()
