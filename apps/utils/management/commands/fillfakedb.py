# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection
from apps.exercises.models import Trainer, Exercise
from apps.diets.models import Diet
from apps.accounts.models import User
from apps.progress.models import ExerciseProgress


def create_trainer(name, family_name, about='', methodic=''):
    trainer = Trainer.objects.create()
    trainer.name = name
    trainer.family_name = family_name
    trainer.about_trainer = about
    trainer.about_methodic = methodic
    trainer.save()


def create_exercise(trainer_name, day, title, desc='', url='', sets=0, repetitions=0, calories=0):
    trainer = Trainer.objects.get(name=trainer_name)
    exercise = Exercise.objects.create(trainer=trainer)
    exercise.day = day
    exercise.title = title
    exercise.description = desc
    exercise.video_url = url
    exercise.prescription_sets = sets
    exercise.prescription_repetitions = repetitions
    exercise.repetition_calories_burned = calories
    exercise.save()


def create_diet(day=0, recommend='какие-то рекоммендации', eat1='приём пищи 1',
                eat2='приём пищи 2', eat3='приём пищи 3', eat4='приём пищи 4', eat5='приём пищи 5'):
    diet = Diet.objects.create(day=day)
    diet.recommend = recommend
    diet.eat1 = eat1
    diet.eat2 = eat2
    diet.eat3 = eat3
    diet.eat4 = eat4
    diet.eat5 = eat5
    diet.save()


def create_progress(username, exercise_id, sets, repetitions):
    user = User.objects.get(username=username)
    progress = ExerciseProgress(user=user, exercise_id=exercise_id)
    progress.sets = sets
    progress.repetitions = repetitions
    progress.save()


def set_random_online_time():
    from random import randint
    users = User.objects.all()
    for user in users:
        user.minutes_online = randint(100, 200)
        user.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        call('python manage.py resetdb', shell=True)

        create_trainer('тренер1', 'фамилия1', 'хз', 'хз')
        create_trainer('тренер2', 'фамилия2', 'хз2', 'хз2')

        create_exercise('тренер1', 1, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 1, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 1, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 1, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 1, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 1, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 1, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 1, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 2, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 2, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 2, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 2, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 2, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 2, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 2, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 2, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 3, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 3, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 3, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 3, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 3, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 3, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 3, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 3, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 4, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 4, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 4, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 4, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 4, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 4, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 4, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 4, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 5, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 5, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 5, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 5, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 5, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 5, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 5, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 5, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 6, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 6, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 6, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 6, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 6, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 6, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 6, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 6, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 7, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 7, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 7, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 7, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 7, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 7, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 7, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 7, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер1', 8, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 8, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер1', 8, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 8, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер1', 8, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 8, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер1', 8, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер1', 8, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        create_exercise('тренер2', 1, 'упр1', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер2', 1, 'упр2', 'парампампам', '#', 5, 5, 0.02)
        create_exercise('тренер2', 1, 'упр3', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер2', 1, 'упр4', 'парампампам', '#', 5, 5, 0.06)
        create_exercise('тренер2', 1, 'упр5', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер2', 1, 'упр6', 'парампампам', '#', 5, 5, 0.04)
        create_exercise('тренер2', 1, 'упр7', 'парампампам', '#', 5, 5, 0.01)
        create_exercise('тренер2', 1, 'упр8', 'парампампам', '#', 5, 5, 0.01)

        for i in range(1, 9):
            create_diet(i)

        from random import randint
        for i in range(1, 25):
            create_progress('admin', randint(1, 64), randint(1, 5), randint(1, 5))

        set_random_online_time()
