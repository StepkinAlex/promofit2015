# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.progress.models import Badge

def db_reset():
    db_params = settings.DATABASES['default']
    db = _mysql.connect(user=db_params['USER'], passwd=db_params['PASSWORD'])
    db.query('drop database if exists %s;' % db_params['NAME'])
    db.query('create database %s charset utf8;' % db_params['NAME'])


def create_user(username, email, password, staff=False, su=False):
    User = get_user_model()
    user = User.objects.create_user(username, email, password)
    user.is_staff = staff
    user.is_superuser = su
    user.sex = 'f'
    user.save()


def create_admin(username, email, password, staff=False, su=False):
    create_user(username, email, password, True, True)

def create_badge(code, name, description=''):
    badge = Badge(code=code, name=name)
    badge.description = description
    badge.save()

class Command(BaseCommand):

    def handle(self, *args, **options):
        db_reset()
        call('python manage.py migrate', shell=True)
        create_admin('admin', 'admin@example.com', '1076351575714795')
        # create_user('sastepkin@yandex.ru', 'sastepkin@yandex.ru', 'aq1')

        create_badge('pioneer', 'Первооткрыватель', 'После одного дня пребывания в приложении')
        create_badge('purposeful', 'Целеустремлённая', 'После 2х дней подряд пребывания в приложении')
        create_badge('seeker', 'Искательница', 'После 3х дней подряд пребывания в приложении')
        create_badge('victress', 'Победительница', 'После 4х дней подряд пребывания в приложении')
        create_badge('celebrity', 'Знаменитость', 'После 7ми дней подряд пребывания в приложении')
        create_badge('legend', 'Легенда', 'После 14ти дней подряд пребывания в приложении')

        create_badge('cheerleader', 'Болельщица', 'За просмотр программы питания в течение двух любых дней')
        create_badge('fitness master', 'Фитнес Мастер', 'За просмотр страницы с упражнениями. (Нужно собрать 2 часа просмотра страниц упражнений)')
        create_badge('record holder', 'Рекордсменка', 'За просмотр 10 видеозаписей упражнений до конца')
        create_badge('fitnes guru', 'Фитнес Гуру', 'За сжигание 700 калорий')
        create_badge('champion', 'Чемпионка', 'За сжигание 2000 калорий за всё время')
        create_badge('fitnes abonement', 'Фитнес абонемент', 'За шару результата дня в соцсетях')

        call('python manage.py createcachetable', shell=True)
