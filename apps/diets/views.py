from django.shortcuts import render

from apps.accounts.views import get_day_user_in_system
from apps.exercises.models import Trainer
from .models import Diet


def get_diets(day):
    if day > 0:
        diets = [Diet.objects.get(day=day)]
    else:
        diets = Diet.objects.all()

    return diets



def get_diet_today(request):
    context = {'user': request.user}

    day = get_day_user_in_system(request.user)
    context['diets'] = get_diets(day)

    return render(request, 'blank.html', context)
