from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# from apps import accounts
# from apps.accounts.views import get_day_user_in_system
from .models import Trainer, Exercise, ExercisesDayIntro


@login_required
def home(request):
    context = {
        'user': request.user,
        'trainers': Trainer.objects.all()
    }
    return render(request, 'home.html', context)


def trainers(request):
    pass


from datetime import datetime


def get_day_user_in_system(user):
    now_date = datetime.now().date()
    delta = now_date - user.start_traning_date.date()
    return delta.days + 1


@login_required
def trainer_exercises(request, trainer_id):
    day = get_day_user_in_system(request.user)
    trainer = Trainer.objects.get(id=trainer_id)
    exercises = Exercise.objects.filter(trainer=trainer, day=day)
    additional_videos = ExercisesDayIntro.objects.get(trainer=trainer, day=day)

    context = {
        'user': request.user,
        'trainer': trainer,
        'day': day,
        'exercises': exercises,
        'videos': additional_videos,
    }
    return render(request, 'exercises.html', context)
