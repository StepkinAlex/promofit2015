from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.exercises.views',
    url(r'^exercises/(?P<trainer_id>[\w]+)/$', 'trainer_exercises', name='exercises'),
)