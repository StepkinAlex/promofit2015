# coding: utf-8
from django.conf import settings
from django.contrib import messages
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.core.mail import send_mail

from .models import User, generate_activation_code
from .forms import UserForm, RegistrationForm

from datetime import datetime


def logout_view(request):
    logout(request)
    return redirect('/')


class LoginBaseView(View):

    def try_login(self, request):
        username = request.POST['username']
        password = request.POST['password']

        if '@' in username:
            try:
                user = get_user_model().objects.get(username=username)
                username = user.username
            except get_user_model().DoesNotExist:
                pass

        error = None
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return
            else:
                error = u'Учетная запись отключена.'
        else:
            error = u'Неверный логин или пароль.'
        return error


class LoginView(LoginBaseView):

    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        error = self.try_login(request)
        if not error:
            return redirect(settings.LOGIN_REDIRECT_URL)
        messages.error(request, error)
        return render(request, 'login.html')


login_view = LoginView.as_view()


class ProfileView(View):

    def get(self, request):
        form = UserForm(instance=request.user)
        return render(request, 'profile.html', {
            'user': request.user,
            'form': form,
        })

    def post(self, request):
        form = UserForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            if request.FILES.get('avatar'):
                user = request.user
                user.avatar = request.FILES.get('avatar')
                user.save()

            form.save()
            msg = 'Изменения сохранены.'
            messages.info(request, msg)
            return redirect(reverse('accounts:profile'))
        return render(request, 'profile.html', {
            'user': request.user,
            'form': form,
        })


profile_view = ProfileView.as_view()


def registration(request):

    def send_activation_email(user, original_password):
        subj = 'Активация аккаунта.'
        activation_url = 'http://%s/activation/%s/' % (request.get_host(), user.activation_key)
        context = {
            'user': user,
            'password': original_password,
            'activation_url': activation_url
        }
        msg = render_to_string('email/activation.html', context)
        send_email(user, subj, msg)

    form = RegistrationForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = User.objects.create_user(
                form.cleaned_data['email'],
                form.cleaned_data['email'],
                form.cleaned_data['password'],
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
            )
            user.is_active = False
            user.save()
            try:
                send_activation_email(user, form.cleaned_data['password'])
            except Exception:
                msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
                messages.error(request, msg)
            else:
                msg = 'На указанный почтовый ящик отправлено письмо с ссылкой для активации'
                messages.info(request, msg)
                return render(request, 'registration-done.html')
        else:
            msg = 'Не правильно заполнена форма.'
            messages.error(request, msg)

    return render(request, 'registration.html', {'form': form})


def activation(request, activation_key):

    def send_activation_done_by_email(user):
        subj = 'Активация выполнена.'
        msg = render_to_string('email/activation_done.html', {'user': user})
        send_email(user, subj, msg)

    try:
        user = User.objects.get(activation_key=activation_key)
    except User.DoesNotExist:
        msg = 'Неверный код активации.'
        messages.error(request, msg)
        return redirect('/')

    if user:
        user.is_active = True
        user.activation_key = generate_activation_code()
        user.save()
        msg = 'Активация выполнена. Учётная запись активна.'
        messages.info(request, msg)

        try:
            send_activation_done_by_email(user)
        except Exception:
            pass

        return redirect('/')
    else:
        msg = 'Активация провалена. Воспользуйтесь ссылкой, которая была Вам выслана по почте!'
        messages.error(request, msg)
        return redirect('/')


def password_reset(request):
    def send_reset_password_activation_email(user):
        subj = 'Активация нового пароля'
        activation_url = 'http://%s/password/reset/%s/' % (request.get_host(), user.activation_key)
        context = {
            'user': user,
            'activation_url': activation_url
        }
        msg = render_to_string('email/reset_password_activation.html', context)
        send_email(user, subj, msg)

    if request.method == 'POST':
        email = request.POST.get('email')
        if not email:
            msg = 'Введите email'
            messages.error(request, msg)
            return render(request, 'reset_password.html')

        try:
            user = User.objects.get(username=email)
        except User.DoesNotExist:
            msg = 'Внимательно вводите email. На введённый вами не зарегистрирован ни один пользователь'
            messages.error(request, msg)
            return render(request, 'reset_password.html')
        else:
            if not user:
                msg = 'Введите email'
                messages.error(request, msg)
                return render(request, 'reset_password.html')

        try:
            send_reset_password_activation_email(user)
        except:
            msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
            messages.error(request, msg)
        else:
            msg = 'На указанный почтовый ящик отправлено письмо с ссылкой для активации нового пароля'
            messages.info(request, msg)
            return render(request, 'home.html')

    return render(request, 'reset_password.html')


def password_reset_done(request, activation_key):
    def send_message(user, password):
        subj = 'Новый пароль.'
        context = {
            'user': user,
            'password': password
        }
        msg = render_to_string('email/reset_password.html', context)
        send_email(user, subj, msg)

    try:
        user = User.objects.get(activation_key=activation_key)
    except User.DoesNotExist:
        msg = 'Неверный код активации для смены пароля.'
        messages.error(request, msg)
        return redirect('/')
    else:
        if not user:
            msg = 'Неверный код активации для смены пароля.'
            messages.error(request, msg)
            return redirect('/')

    import random
    new_password = ''.join(
        [random.choice(list('1234567890abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXY')) for x in xrange(8)])
    user.set_password(new_password)
    user.activation_key = generate_activation_code()
    user.save()
    try:
        send_message(user, new_password)
    except:
        msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
        messages.error(request, msg)
    else:
        msg = 'На указанный почтовый ящик отправлено письмо с ссылкой для активации'
        messages.info(request, msg)
        return render(request, 'reset_password-done.html')

    return render(request, 'reset_password.html')


def send_email(user, subj, msg):
    send_mail(subj, msg, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message=msg)


def get_day_user_in_system(user):
    now_date = datetime.now().date()
    delta = now_date - user.start_traning_date.date()
    return delta.days + 1
