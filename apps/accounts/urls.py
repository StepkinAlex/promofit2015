from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.accounts.views',
    url(r'^registration/$', 'registration', name='registration'),
    url(r'^activation/(?P<activation_key>[\w]+)/$', 'activation', name='activation'),
    url(r'^login/$', 'login_view', name='login'),
    url(r'^logout/$', 'logout_view', name='logout'),
    url(r'^password/reset/$', 'password_reset', name='password_reset'), 
    url(r'^password/reset/(?P<activation_key>[\w]+)/$', 'password_reset_done', name='password_reset_done'), 
      
    url(r'^profile/$', 'profile_view', name='profile'),
)