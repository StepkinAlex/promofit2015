# coding: utf-8
import json
import pytest
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from .test_fixtures import user


@pytest.mark.django_db
def test_login_error(client, user):
    response = client.post(reverse('accounts:login'), data={
        'username': user.username,
        'password': 'wrong',
    })
    assert response.status_code == 200
    assert 'Неверный логин или пароль.' in response.content


@pytest.mark.django_db
def test_login_success(client, user):
    response = client.post(reverse('accounts:login'), data={
        'username': user.username,
        'password': user.password_raw,
    })
    assert response.status_code == 302
    assert response.content == ''


@pytest.mark.django_db
def test_logout(client, user):
    client.login(username=user.username, password=user.password_raw)
    response = client.get(reverse('accounts:logout'))
    assert response.status_code == 302
