# coding: utf-8
import datetime
import pytest
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from .test_fixtures import user


@pytest.mark.django_db
def test_profile_view(user, client):
    client.login(username=user.username, password=user.password_raw)
    response = client.get(reverse('accounts:profile'))
    assert response.status_code == 200


@pytest.mark.django_db
def test_profile_post(user, client):
    client.login(username=user.username, password=user.password_raw)
    response = client.post(reverse('accounts:profile'), data={
        'email': 'vasya@hotmail.com',
        'last_name': 'pupkin',
        'first_name': 'vasiliy',
        'age': '20',
        'sex': 'm',
        'city': 'Novosib',
        'about': 'Test user',
        'weight': 90,
        'growth': 180,
    })
    assert response.status_code == 302
    new_user = get_user_model().objects.get(id=user.id)
    assert new_user.email == 'vasya@hotmail.com'
    assert new_user.last_name == 'pupkin'
    assert new_user.first_name == 'vasiliy'
    assert new_user.age == 20
    assert new_user.sex == 'm'
    assert new_user.city == 'Novosib'
    assert new_user.weight == 90
    assert new_user.growth == 180
