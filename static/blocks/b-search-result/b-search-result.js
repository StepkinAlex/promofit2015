$(function() {
  $('.b-search-result').justifiedGallery({
    rowHeight: 200,
    maxRowHeight: 300,
    lastRow: 'justify',
    margins: 8
  });
});
