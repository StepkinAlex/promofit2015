$(function() {
  $('.b-photo-group__photos').justifiedGallery({
    rowHeight: 180,
    lastRow: 'justify',
    margins: 8
  });
});
