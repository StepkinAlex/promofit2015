$(function() {
	$('.b-top-search__example-query').on('click', function(){
		var exampleText = $(this).text();
		$('.b-top-search__field-text').val(exampleText);
	});

	$('#b-top-search__select-author').selectize();
	$('#b-top-search__select-source').selectize();

	$(window).on('load resize', function(){
		var windowWidth = parseInt($(window).width());
		if (windowWidth<1200) {
			$('.b-top-search').addClass('b-top-search_min_yes');
		} else {
			$('.b-top-search').removeClass('b-top-search_min_yes');
		}
	});

	$('.b-top-search__addition-extended').on('click', function(){
		$('.b-top-search').removeClass('b-top-search_min_yes');
	});

});