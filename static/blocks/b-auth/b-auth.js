$(function() {
	$('.b-auth__close').on('click', function(){
		closePopup();
	});

	$('.b-head__actions-enter').on('click', function(e){
		e.preventDefault();
		showPopup();
	});

	function closePopup(){
		$('.b-body__content').removeClass('b-body__content_blurred_yes');
		$('.b-body__overlay').removeClass('b-body__overlay_show_yes');
		$('.b-auth').removeClass('b-auth_show_yes');
	}

	function showPopup(){
		$('.b-body__content').addClass('b-body__content_blurred_yes');
		$('.b-body__overlay').addClass('b-body__overlay_show_yes');
		$('.b-auth').addClass('b-auth_show_yes');
	}

});