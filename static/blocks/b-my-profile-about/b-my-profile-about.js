$(function() {
	function showInfo(){
		$('.b-my-profile-about__links').removeClass('b-my-profile-about__links_hide_yes');
		$('.b-my-profile-about').removeClass('b-my-profile-about_hide_yes');
		$('.b-my-profile-form').removeClass('b-my-profile-form_show_yes');
	}

	function editInfo(){
		$('.b-my-profile-about__links').addClass('b-my-profile-about__links_hide_yes');
		$('.b-my-profile-about').addClass('b-my-profile-about_hide_yes');
		$('.b-my-profile-form').addClass('b-my-profile-form_show_yes');
	}

	$('.b-my-profile-about__edit-link').on('click', function(e){
		e.preventDefault();
		var isEdit = $('.b-my-profile-form').hasClass('b-my-profile-form_show_yes');
		if (!isEdit) {
			editInfo();
		}
	});
});
