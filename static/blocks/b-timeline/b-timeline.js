$(function() {
	$('.b-timeline__slider').noUiSlider({
		start: [ 1863, 1975 ],
		step: 1,
		connect: true,
		range: {
			'min': [  1860 ],
			'max': [ 2000 ]
		}
	});

	$('.b-timeline__slider').Link('lower').to('-inline-<div class="b-timeline__year"></div>', function ( value ) {
		$(this).html(
			'<div class="b-timeline__dot"></div><div class="b-timeline__bg"></div>' + '<div class="b-timeline__year-text">' +parseInt(value, 10) + '</div>'
		);
	});

	$('.b-timeline__slider').Link('upper').to('-inline-<div class="b-timeline__year"></div>', function ( value ) {
		$(this).html(
			'<div class="b-timeline__dot"></div><div class="b-timeline__bg"></div>' + '<div class="b-timeline__year-text">' +parseInt(value, 10) + '</div>'
		);
	});

});