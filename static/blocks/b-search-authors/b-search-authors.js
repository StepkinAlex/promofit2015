$(function() {
	$('.b-search-authors__all-authors').on('click', function(){
		var _this = $(this),
			isHide = $('.b-search-authors__hidden').hasClass('b-search-authors__hidden_hide_yes');
			showtext = _this.data('showtext'),
			hidetext = _this.data('hidetext');
		if (isHide) {
			$('.b-search-authors__hidden').removeClass('b-search-authors__hidden_hide_yes');
			$('.b-search-authors__all-authors').text(hidetext);
		} else {
			$('.b-search-authors__hidden').addClass('b-search-authors__hidden_hide_yes');
			$('.b-search-authors__all-authors').text(showtext);
		}
	});
});