$(function() {
	$('.b-search-tags__all-tags').on('click', function(){
		var _this = $(this),
			isHide = $('.b-search-tags__hidden').hasClass('b-search-tags__hidden_hide_yes');
			showtext = _this.data('showtext'),
			hidetext = _this.data('hidetext');
		if (isHide) {
			$('.b-search-tags__hidden').removeClass('b-search-tags__hidden_hide_yes');
			$('.b-search-tags__all-tags').text(hidetext);
		} else {
			$('.b-search-tags__hidden').addClass('b-search-tags__hidden_hide_yes');
			$('.b-search-tags__all-tags').text(showtext);
		}
	});
});