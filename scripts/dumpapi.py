#!/usr/bin/env python
# coding: utf-8
import json
import sys
import requests


url = sys.argv[1]
response = requests.get(url)
data = json.loads(response.text)
print json.dumps(data, ensure_ascii=False, indent=4).encode('utf-8')
